"use strict";

const crypto = require("crypto"),
  pw = require("pw");

const ALGORITHM = "aes-256-gcm";
const IV_LENGTH = 16; // For AES, this is always 16
let ENCRYPTION_KEY = undefined; //"12345678901234567890123456789012"; // Must be 256 bytes (32 characters)

/*
* Ajouter creation d'un user avec passeord a 40 charachters
plus bcrypt 40 round
*/

const cryptoService = {
  enterPassword: function() {
    return new Promise((resolve, reject) => {
      process.stdout.write("🔑 Enter Your Password: ");
      pw(password => {
        ENCRYPTION_KEY = password;

        resolve(true);
      });
    });
  },

  encrypt: function(text) {
    return new Promise((resolve, reject) => {
      const iv = crypto.randomBytes(IV_LENGTH);
      let cipher = crypto.createCipheriv(
        ALGORITHM,
        new Buffer(ENCRYPTION_KEY),
        iv
      );

      let encrypted = cipher.update(JSON.stringify(text), "utf8", "hex");
      encrypted += cipher.final("hex");
      let tag = cipher.getAuthTag();

      resolve({ content: encrypted, iv, tag });
    });
  },

  decrypt: function(encrypted) {
    return new Promise((resolve, reject) => {
      const iv = encrypted.iv.buffer;
      let decipher = crypto.createDecipheriv(
        ALGORITHM,
        new Buffer(ENCRYPTION_KEY),
        iv
      );
      decipher.setAuthTag(encrypted.tag.buffer);
      let decrypted = decipher.update(encrypted.content, "hex", "utf8");
      decrypted += decipher.final("utf8");

      resolve(JSON.parse(decrypted));
    });
  }
};

module.exports = cryptoService;
